using NUnit.Framework;
using System;

namespace Nunit.Demo.Test
{
    [TestFixture]
    public class Tests
    {
        Calculator calc;
        [SetUp]
        public void SetUp()
        {
            calc = new Calculator();
        }


        [TearDown]  
        public void TearDown()
        {
            calc.Dispose();
        }
        [Test]
        public void Test_Addition_With_Valid_Integers()
        {
            //var calc = new Calculator();
            var result = calc.Addition(3,5);
            Assert.AreEqual(8, result);
        }

        [Test]
        public void Test_Substraction_Argument_Exception()
        {
            //var calc = new Calculator();
            
            //Assert.Catch<SystemException>(()=> calc.Substruction(4,5));
            Assert.Throws<ArgumentException>(() => calc.Substruction(4, 5));
        }

        [Ignore("Test_Ignore")]
        public void Test_Ignore()
        {
            Assert.Pass();
        }
    }
}