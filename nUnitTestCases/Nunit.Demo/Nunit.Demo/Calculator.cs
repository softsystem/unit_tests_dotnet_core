﻿using System;

namespace Nunit.Demo
{
    public class Calculator : IDisposable
    {
        public int Addition(int fist, int second)
        {
            return fist + second;   
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public int Substruction(int first, int second)
        {
            if (first < second)
            {
                throw new ArgumentException($"first number {first} is less then second number {second} ");
            }               

            return first - second;
        }
    }
}
