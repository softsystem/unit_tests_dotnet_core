CREATE DATABASE BlogDB
USE BlogDB

CREATE TABLE Post
(
	PostId INT IDENTITY PRIMARY KEY,
	Title VARCHAR(250),
	Description Varchar(250),
	 CategoryId int FOREIGN KEY references  Category(Id),
	CreatedDate DateTime
)

CREATE TABLE Category
(
	Id INT IDENTITY PRIMARY KEY,
	Name  VARCHAR(250),
	Slug  Varchar(250)
)